package algorithms_cj.code.algorithms.strings

import std.collection.*

public class Permutations {
    private static func _permutations(source: String): HashSet<String> {
        var perms = HashSet<String>()
        var RArray = source.toRuneArray()
        if (source.size == 1) {
            perms.put(source)
        } else if (source.size > 1) {
            var lastIndex = source.size - 1
            var lastChar = RArray[lastIndex]
            var lastCharArray: Array<Rune> = [lastChar]
            var subString = source[0..lastIndex]
            perms = _mergePermutations(_permutations(subString), String(lastCharArray))
        }

        return perms
    }

    private static func _mergePermutations(permutations: HashSet<String>, character: String): HashSet<String> {
        var merges = HashSet<String>()
        for (perm in permutations) {
            for (i in 0..perm.size) {
                var newMerge = insert(perm, i, character)
                if (!merges.contains(newMerge)) {
                    merges.put(newMerge)
                }
            }
        }

        return merges
    }

    public static func ComputeDistinct(source: String): HashSet<String> {
        return _permutations(source)
    }

    public static func isAnargram(source: String, other: String): Bool {
        if (source.isEmpty() || other.isEmpty()) {
            return false
        }
        if (source.size != other.size) {
            return false
        }
        if (source == other) {
            return true
        }

        let len = source.size
        var hashSetSource = HashSet<Byte>()
        var hashSetOther = HashSet<Byte>()
        for (i in 0..len) {
            hashSetSource.put(source[i])
            hashSetOther.put(other[i])
        }
        for (i in 0..len) {
            if (!hashSetSource.contains(other[i]) || !hashSetOther.contains(source[i])) {
                return false
            }
        }
        return true
    }

    private static func insert(destination: String, startIndex: Int64, source: String): String {
        var prefix = destination[0..startIndex].toString()
        var suffix = destination[startIndex..destination.size].toString()
        return prefix + source + suffix
    }
}

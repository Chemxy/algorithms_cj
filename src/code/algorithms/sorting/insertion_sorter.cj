package algorithms_cj.code.algorithms.sorting

import std.collection.ArrayList
import algorithms_cj.code.algorithms.common.*

public class InsertionSorter<T> where T <: Comparable<T> {
    public static func InsertionSort(_collection: ArrayList<T>, _comparer!: Comparers<T> = Comparers<T>.default) {
        InsertionSortAscending(_collection, _comparer: _comparer)
    }

    public static func InsertionSortAscending(_collection: ArrayList<T>, _comparer!: Comparers<T> = Comparers<T>.default) {
        for (i in 1.._collection.size) {
            var value = _collection[i]
            var j = i - 1
            while ((j >= 0) && (_comparer.compare(_collection[j], value) == Ordering.GT)) {
                _collection[j + 1] = _collection[j]
                j = j - 1
            }
            _collection[j + 1] = value
        }
    }

    public static func InsertionSortDescending(_collection: ArrayList<T>,
        _comparer!: Comparers<T> = Comparers<T>.default) {
        for (i in 1.._collection.size) {
            var value = _collection[i]
            var j = i - 1
            while ((j >= 0) && (_comparer.compare(_collection[j], value) == Ordering.LT)) {
                _collection[j + 1] = _collection[j]
                j = j - 1
            }
            _collection[j + 1] = value
        }
    }

    public static func InsertionSort(_collection: Array<T>, _comparer!: Comparers<T> = Comparers<T>.default) {
        InsertionSortAscending(_collection, _comparer: _comparer)
    }

    public static func InsertionSortAscending(_collection: Array<T>, _comparer!: Comparers<T> = Comparers<T>.default) {
        for (i in 1.._collection.size) {
            var value = _collection[i]
            var j = i - 1
            while ((j >= 0) && (_comparer.compare(_collection[j], value) == Ordering.GT)) {
                _collection[j + 1] = _collection[j]
                j = j - 1
            }
            _collection[j + 1] = value
        }
    }

    public static func InsertionSortDescending(_collection: Array<T>, _comparer!: Comparers<T> = Comparers<T>.default) {
        for (i in 1.._collection.size) {
            var value = _collection[i]
            var j = i - 1
            while ((j >= 0) && (_comparer.compare(_collection[j], value) == Ordering.LT)) {
                _collection[j + 1] = _collection[j]
                j = j - 1
            }
            _collection[j + 1] = value
        }
    }
}
